from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
author = "Patricia Christiana"

def index(request):
    response = {"author": author}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
