from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Halo perkenalkan! Nama saya Patricia Christiana, Saya lahir pada 10 November 1998. Saat ini saya duduk di bangku semester 3 FASILKOM UI prodi Sistem Informasi!'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)