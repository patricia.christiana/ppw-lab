import requests
import os

API_MAHASISWA_LIST_URL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa-list/"

class CSUIhelper:
    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password
        self.client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
        self.access_token = self.get_access_token()

    def get_access_token(self):
        if self.username != None and self.password != None:
            url = "https://akun.cs.ui.ac.id/oauth/token/"
            payload = "username=" + self.username + "&password=" + \
                self.password + "&grant_type=password"
            headers = {
                'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
                'cache-control': "no-cache",
                'content-type': "application/x-www-form-urlencoded"
            }

            response = requests.request(
                "POST", url, data=payload, headers=headers)

            if(response.json().get("access_token") != None):
                self.access_token = response.json()["access_token"]
            else:
                self.access_token = None
            return response.json().get("access_token")

        return None

    def get_client_id(self):
        return self.client_id

    def get_auth_param_dict(self):
        dict = {}
        acces_token = self.get_access_token()
        client_id = self.get_client_id()
        dict['access_token'] = acces_token
        dict['client_id'] = client_id

        return dict

    def get_mahasiswa_list(self):
        response = requests.get(API_MAHASISWA_LIST_URL,
                                params={"access_token": self.access_token, "client_id": self.client_id})
        self.next_page = response.json()["next"]
        self.previous_page = response.json()["previous"]
        mahasiswa_list = response.json()["results"]
        return mahasiswa_list

    def previous_mahasiswa_list(self):
        response = requests.get(self.previous_page,
                                params={"access_token": self.access_token, "client_id": self.client_id})
        self.next_page = response.json()["next"]
        self.previous_page = response.json()["previous"]
        mahasiswa_list = response.json()["results"]
        return mahasiswa_list

    def next_mahasiswa_list(self):
        response = requests.get(self.next_page,
                                params={"access_token": self.access_token, "client_id": self.client_id})
        self.next_page = response.json()["next"]
        self.previous_page = response.json()["previous"]
        mahasiswa_list = response.json()["results"]
        return mahasiswa_list