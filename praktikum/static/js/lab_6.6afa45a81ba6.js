var head = document.getElementsByClassName('chat-head');
var body = document.getElementsByClassName('chat-body');
var isiTulisan = document.getElementById ('tulisan');
var tempatTulisan = document.getElementsByClassName ('msg-insert');
var sender = true;

$(isiTulisan).keypress(function(e){
	if(e.keyCode === 13){
		if (sender && isiTulisan.value.length > 0){
			$(tempatTulisan).append('<p class = "msg-send">'+ isiTulisan.value + '</p>')
			sender=false;
		}
		else if (!sender && isiTulisan.value.length > 0){
			$(tempatTulisan).append('<p class = "msg-receive">'+ isiTulisan.value + '</p>')
			sender =true;
		}
		
		isiTulisan.value = "";
	}

});

$(head).click(function(){
	$(body).toggle();
})

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
    if (x === 'ac') {
    /* implemetnasi clear all */
        print.value = '';
        erase = true;
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } else if (x === 'log'|| x === 'sin'|| x === 'tan'){
        switch(x){
            case 'log': print.value = Math.log10(print.value);
            break;
            case 'sin': print.value = Math.sin(print.value);
            break;
            case 'tan': print.value = Math.tan(print.value);
            break;
        }
    } else {
        print.value += x;
    }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];


 localStorage.setItem("themes",JSON.stringify(themes));
 var catalog = JSON.parse(localStorage.getItem("themes"));

$.each(catalog, function(index){
    var id = catalog[index].id;
    var Color = catalog[index].text;
    var bgColor = catalog[index].bcgColor;
    var fontColor = catalog[index].fontColor;

    localStorage.setItem(id,Color.concat(",",bgColor,",",fontColor));
})

var selected = JSON.stringify({"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}});
localStorage.setItem("selectedTheme",selected);

$(document).ready(function() {
    $('.my-select').select2({
        placeholder : "Select Color",
        data : themes
    });
});

$('.apply-button').on('click', function(){  // sesuaikan class button
    var theme = localStorage.getItem($(".my-select").val());
    var apply = theme.split(",");

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    document.body.style.backgroundColor = apply[1];
    document.body.style.fontColor = apply[2];

    // [TODO] simpan object theme tadi ke local storage selectedTheme
    var themeNow = "{\""+ apply[0] + "\":{" + "\"bcgColor\":\"" + apply[1]+ "\","
    +"\"fontColor\":\""+ apply[2]+"\"}}";

    localStorage.setItem("selectedItem",themeNow);

    return false;
});

// END

